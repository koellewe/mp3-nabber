package com.sps.mus.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Jans Rautenbach on 08 Jul 2017.
 */

public class LiedjiesDBHelper extends SQLiteOpenHelper{

    public static final int DB_VERSION = 3;     //inc at upgrade
    public static final String DB_NAME = "Liedjies.db";

    public LiedjiesDBHelper(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db){
        db.execSQL(LiedjiesContract.SQL_CREATE_TblLiedjies);
        db.execSQL(LiedjiesContract.SQL_CREATE_TblLinks);
        db.execSQL(LiedjiesContract.SQL_CREATE_TblArtists);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //policy: create any new tables if they don't exist. Leave existing tables as-is
        onCreate(db);
    }

}
