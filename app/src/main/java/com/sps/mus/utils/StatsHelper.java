package com.sps.mus.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Build;

import com.jaredrummler.android.device.DeviceName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Date;

/**
 * Created by koell on 05 Jan 2018.
 */

public class StatsHelper {

    public static final String ACTION_DOWNLOAD = "SONG_DOWNLOAD";
    public static final String ACTION_MP3_SERVER_ISSUE = "MP3_SERVER_GLITCH";
    public static final String ACTION_FAILS_STOP = "FAIL_STOP";
    public static final String ACTION_SHARE_RECEIVED = "SHARE_RECEIVED";
    public static final String ACTION_OPEN_DOWNLOADS = "OPEN_DOWNLOADS";
    public static final String ACTION_NB_ERROR = "BIG_PROBLEM";
    public static final String ACTION_SETTINGS_CHANGE = "SETTINGS_CHANGED";
    public static final String ACTION_SETTINGS_RESET = "SETTINGS_RESET";

    private Context context;
    private int currVersionCode;
    private String GUID;

    public StatsHelper(Context context){
        this.context = context;
        JanTools tmpJT = new JanTools(context);
        currVersionCode = tmpJT.getVersionCode();
        GUID = tmpJT.getGUID();
    }

    public void newStat(String action, JSONObject details){

        SQLiteDatabase statsDB = new StatsDBHelper(context).getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(StatsContract.TableDetails.COLUMN_ACTION, action);
        values.put(StatsContract.TableDetails.COLUMN_DETAILS, details.toString());
        values.put(StatsContract.TableDetails.COLUMN_TIMESTAMP, new Date().getTime());
        values.put(StatsContract.TableDetails.COLUMN_VERSION, currVersionCode);

        statsDB.insert(
                StatsContract.TableDetails.TABLE_NAME,
                null,
                values
        );

        statsDB.close();

    }

    public void uploadStats(){

        SQLiteDatabase statsDB = new StatsDBHelper(context).getReadableDatabase();

        Cursor stats = statsDB.query(
                StatsContract.TableDetails.TABLE_NAME,
                new String[]{
                        StatsContract.TableDetails.COLUMN_TIMESTAMP,
                        StatsContract.TableDetails.COLUMN_ACTION,
                        StatsContract.TableDetails.COLUMN_DETAILS,
                        StatsContract.TableDetails.COLUMN_VERSION
                },
                null, null, null, null, null
        );

        if (stats.getCount() > 0) {

            JSONObject jsonToSend = new JSONObject();
            JSONArray actionsArr = new JSONArray();

            stats.moveToFirst();

            while (!stats.isAfterLast()) {

                JSONObject actionObj = new JSONObject();
                try {
                    actionObj.put("action_timestamp", stats.getLong(0)/1000);
                    actionObj.put("action", stats.getString(1));
                    actionObj.put("details", stats.getString(2));
                    actionObj.put("version", stats.getInt(3) == 0 ? currVersionCode-1 : currVersionCode);
                } catch (JSONException ignored) {}

                actionsArr.put(actionObj);

                stats.moveToNext();

            }

            try {
                jsonToSend.put("GUID", GUID);
                jsonToSend.put("actionsArr", actionsArr);
                jsonToSend.put("versionCode", "variable");
            } catch (JSONException ignored) {}

            new StatsUploader().execute(jsonToSend);

        }else{
            System.out.println("No stats to upload");
        }

        stats.close();
        statsDB.close();

    }

    public void uploadDeviceDetails(){

        try {
            JSONObject jsonToSend = new JSONObject()
                    .put("GUID", GUID)
                    .put("android_release", Build.VERSION.RELEASE)
                    .put("model", DeviceName.getDeviceName(Build.MODEL, Build.MODEL));

            new DevDetesUploader().execute(jsonToSend);

        }catch(JSONException ignored){}

    }

    private class StatsUploader extends AsyncTask<JSONObject, Void, Void>{

        @Override
        protected Void doInBackground(JSONObject... jsonToSend) {

            try {
                JSONObject result = new RautenAPI(RautenAPI.URL_STATS).makeReq(jsonToSend[0], false);
                if (result.getBoolean("success")) {
                    System.out.println("Stats uploaded.");
                    new StatsDBHelper(context).getWritableDatabase().
                            delete(StatsContract.TableDetails.TABLE_NAME, null, null);

                }else{
                    System.out.println("Stats upload failed: " + result.getString("failMsg"));
                }

            } catch (IOException | JSONException e) {
                System.out.println("Could not upload stats: " + e.getMessage());
            }

            return null;

        }
    }

    private static class DevDetesUploader extends AsyncTask<JSONObject, Void, Void>{

        @Override
        protected Void doInBackground(JSONObject... jsonToSend) {

            try{

                JSONObject result = new RautenAPI(RautenAPI.URL_DEVICE_DETAILS).makeReq(jsonToSend[0], false);

                if (result.getBoolean("success")){
                    System.out.println("Device details uploaded");
                }else{
                    System.out.println("Device details couldn't be uploaded: " + result.getString("failMsg"));
                }

            }catch (JSONException|IOException e){
                System.out.println("Couldn't upload device details: " + e.getMessage());
            }

            return null;

        }
    }


}
