package com.sps.mus.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by koell on 17 Oct 2017.
 */

public class RautenAPI {

    public static final String URL_VERSION = "https://apps.rauten.co.za/mp3-nabber/latest_version.php";
    public static final String URL_STATS = "https://apps.rauten.co.za/mp3-nabber/stats.php";
    public static final String URL_FEEDBACK = "https://apps.rauten.co.za/mp3-nabber/feedback.php";
    public static final String URL_DEVICE_DETAILS = "https://apps.rauten.co.za/mp3-nabber/device_details.php";

    private String connUrl;

    public RautenAPI(String connURL){
        this.connUrl = connURL;
    }

    public JSONObject makeReq(JSONObject jsonToSend) throws JSONException, IOException {return makeReq(jsonToSend, true);}
    public JSONObject makeReq(JSONObject jsonToSend, boolean doLog) throws JSONException, IOException{

        HttpURLConnection http = ((HttpURLConnection) new URL(connUrl).openConnection());
        http.setRequestMethod("POST");
        http.setConnectTimeout(3_000);
        http.setReadTimeout(8_000);
        http.setRequestProperty("Content-Type", "application/json");
        http.setRequestProperty("Accept", "application/json");
        http.setDoOutput(true);
        http.setDoInput(true);

        OutputStreamWriter sender = new OutputStreamWriter(http.getOutputStream());
        sender.write(jsonToSend.toString());
        sender.flush();
        sender.close();

        if (doLog) {
            if (jsonToSend.toString().length() < 1000) {
                System.out.println("SENT: " + jsonToSend.toString());
            } else {
                System.out.println("Request SENT.");
            }
        }

        int responseCode = http.getResponseCode();
        if (responseCode != HttpURLConnection.HTTP_OK) {
            throw new IOException("Server responded with non-OK HTTP code");
        }else{

            StringBuilder responseRaw = new StringBuilder();
            BufferedReader receiver = new BufferedReader(new InputStreamReader(http.getInputStream()));
            String line;
            while ((line = receiver.readLine()) != null){
                responseRaw.append(line);
            }
            receiver.close();

            if (doLog) {
                if (responseRaw.toString().length() < 1000) {
                    System.out.println("RECEIVED: " + responseRaw.toString());
                } else {
                    System.out.println("Response RECEIVED.");
                }
            }

            return new JSONObject(responseRaw.toString());

        }


    }

}
