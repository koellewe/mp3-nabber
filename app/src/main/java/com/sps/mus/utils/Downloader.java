package com.sps.mus.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.StringRes;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.SearchResult;
import com.sps.mus.MainActivity;
import com.sps.mus.R;
import com.sps.mus.SettingsActivity;

import org.apache.commons.lang3.ArrayUtils;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.myid3.MyID3;
import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by koell on 21 Jan 2018.
 */

public class Downloader extends AsyncTask<Void, Integer, Void> {

    private static int FAILACT_STOP = 2;
    private static int FAILACT_SKIP = 1;
    private static int FAILACT_RETRY = 0;
    private static final String YT_API_KEY = "AIzaSyB1vCWCTwLR5pOv1o4lhs-fjLJtKu_wclk";

    public static final String EVENT_CANCEL = "EVENT_CANCEL";
    public static final String EVENT_PROG_UPDATE = "EVENT_PROGRESS_UPDATE";
    public static final String EVENT_PROG_REQUEST = "EVENT_PROGRESS_REQUEST";

    public static final String EXTRA_SUBEVENT = "sub-event";
    public static final String EXTRA_TOAST_MSG = "toast-message";
    public static final String EXTRA_NEW_DL_MODE = "new-dl-mode";
    public static final String EXTRA_NEW_PROG_TXT = "new-progress-text";
    public static final String EXTRA_NEW_PROG_RESID = "new-progress-resid";
    public static final String EXTRA_FILE_PATH = "file-path";
    public static final String EXTRA_PROG_PERC = "progress-percentage";

    public static final String SUBEVENT_LIST_UPDATE = "update-liedjie-lys";
    public static final String SUBEVENT_TOAST = "make-toast";
    public static final String SUBEVENT_CHANGE_DL_MODE = "change-dl-mode";
    public static final String SUBEVENT_CHANGE_PROG_TXT = "change-progress-text";
    public static final String SUBEVENT_REQ_STORAGE = "request-storage-permission";
    public static final String SUBEVENT_SCAN_FILE = "media-scan-file";
    public static final String SUBEVENT_CHANGE_PROG_BAR = "change-progress-bar";
    public static final String SUBEVENT_FAILS_DIALOG = "show-failure-dialog";

    private boolean retry = false;
    private String songTitleFull = null;
    private DateTime startPoint;
    private LiedjiesHelper liedjiesHelper;
    private WeakReference<Activity> activityRef;
    private JanTools jantools;
    private NotifHelper notifHelper;
    private StatsHelper statsHelper;
    private File outputDir;
    private int consecutiveFails = 0;
    private String mbs = null;

    public Downloader(Activity activity){
        activityRef = new WeakReference<>(activity);
        liedjiesHelper = new LiedjiesHelper(activity);
        jantools = new JanTools(activity);
        notifHelper = new NotifHelper(activity);
        statsHelper = new StatsHelper(activity);
        outputDir = getOutputDir(activity);

        LocalBroadcastManager.getInstance(activity).registerReceiver(
                cancelReceiver, new IntentFilter(EVENT_CANCEL)
        );
        LocalBroadcastManager.getInstance(activity).registerReceiver(
                progReqReceiver, new IntentFilter(EVENT_PROG_REQUEST)
        );
    }

    private BroadcastReceiver progReqReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (songTitleFull != null){
                String newProg;
                if (mbs == null) {
                    newProg = (songTitleFull);
                } else {
                    if (mbs.equals("0")){
                        newProg = (songTitleFull);
                    }else {
                        newProg = (String.format("%s (%sMB)", songTitleFull, mbs));
                    }
                }

                sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                        .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_PROG_TXT)
                        .putExtra(EXTRA_NEW_PROG_TXT, newProg)
                );
            }

            if (lastPublishedProgress > -1){
                sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                        .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_PROG_BAR)
                        .putExtra(EXTRA_PROG_PERC, lastPublishedProgress)
                );
            }

        }
    };

    private BroadcastReceiver cancelReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            cancel(false);
        }
    };

    private void sendBroadcast(Intent intent){
        if (activityRef.get() != null){
            LocalBroadcastManager.getInstance(activityRef.get()).sendBroadcast(intent);
        }
    }

    /** Quick notes:
     *  returning null will stop the downloads and leave the failed download in the list
     *      it will delete the file if it was written (partially)
     *
     *  returning the params will be considered as a successful download
     *
     *  returning the params AND setting retry to true will retry the download
     *      it will also delete the file
     *
     *  executing moveToBottom() and setting retry to true effectively skips the song
     */
    protected Void doInBackground(Void... v){

        while (liedjiesHelper.getLiedjieDBLen() > 0 && !safeIsCancelled()){

            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_NEW_DL_MODE, MainActivity.ActMode.DOWNLOADING)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_DL_MODE)
            );

            startPoint = new DateTime();
            publishProgress(0);

            jantools.writePref(JanTools.PREF_ONGOING_DL, "true");

            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_PROG_TXT)
                    .putExtra(EXTRA_NEW_PROG_RESID, R.string.notif_getting_ready_to_dl)
            );

            String[] songToDL = liedjiesHelper.getTopSong();

            if ( !jantools.checkConnection() ){
                toast(R.string.no_internet);
                failProperly(FAILACT_STOP, songToDL);
                continue;
            }

            //STEP 1: Get YT Link

            String videoID = null; String videoTitle = null;

            String preSavedUrl = liedjiesHelper.getPreSavedUrl(songToDL[0], songToDL[1]);
            if (preSavedUrl != null){

                System.out.println("Url pre-saved: " + preSavedUrl);
                videoID = preSavedUrl.substring(preSavedUrl.lastIndexOf('=')+1);
                videoTitle = songToDL[0];

            }else {

                try {
                    YouTube youtube = new YouTube.Builder(Auth.HTTP_TRANSPORT, Auth.JSON_FACTORY, new HttpRequestInitializer() {
                        public void initialize(HttpRequest request) throws IOException {}
                    }).setApplicationName("com.sps.mus").build();

                    YouTube.Search.List search = youtube.search().list("id,snippet");

                    search.setKey(YT_API_KEY);
                    search.setQ(songToDL[1] + " " + songToDL[0]);
                    search.setType("video");
                    search.setMaxResults(1L);

                    List<SearchResult> searchResponseList = search.execute().getItems();

                    videoID = searchResponseList.get(0).getId().getVideoId();    //take first result as best

                    String ytUrl = "https://youtube.com/watch?v="+videoID;
                    System.out.println("Searched yt and got: " + ytUrl);
                    liedjiesHelper.saveURL(liedjiesHelper.getSongID(songToDL[0], songToDL[1]), ytUrl);

                } catch (IOException e) {
                    e.printStackTrace();
                        toast(R.string.youtube_issue);
                    failProperly(FAILACT_STOP, songToDL);
                    continue;
                }
            }

            if (safeIsCancelled()){
                failProperly(FAILACT_STOP, songToDL);
                continue;
            }

            //STEP 1.5: set full song title & update notification


            if (songToDL[1].equals("-")||songToDL[1].equals("")){
                songTitleFull = songToDL[0];
            }else {
                songTitleFull = songToDL[1] + " - " + songToDL[0];
            }

            notifHelper.updateNotification(songTitleFull);


            //STEP 2: Get dl link

            String ytServiceURL = "https://www.yt-download.org/@grab?vidID="+videoID+"&format=mp3&streams=mp3&api=button";
            String[] songDlLinks;
            try {

                HttpURLConnection http = ((HttpURLConnection) new URL(ytServiceURL).openConnection());
                http.setRequestMethod("GET");
                http.setConnectTimeout(3_000);
                http.setReadTimeout(8_000);
                http.setRequestProperty("Accept", "text/html, */*; q=0.01");
                http.setRequestProperty("Host", "www.yt-download.org");
                http.setRequestProperty("User-Agent", "Rauten/"+jantools.getVersionName());
                http.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
                http.setRequestProperty("Accept-Encodeing", "gzip, deflate, br");
                http.setRequestProperty("X-Requested-With", "XMLHttpRequest");
                http.setRequestProperty("Referer", "https://www.yt-download.org/@api/button/mp3/"+videoID);
                http.setRequestProperty("Cookie", "__cfduid=d3bd2660d633e02f27de346e51d4e37a01510510013; PHPSESSID=s1");  //this might be this app's downfall
                http.setRequestProperty("Connection", "keep-alive");
                http.setDoOutput(false);
                http.setDoInput(true);

                int responseCode = http.getResponseCode();
                if (responseCode != HttpURLConnection.HTTP_OK) {
                    throw new StringIndexOutOfBoundsException("Server responded with HTTP code: " + responseCode);
                }else{

                    StringBuilder responseRaw = new StringBuilder();
                    BufferedReader receiver = new BufferedReader(new InputStreamReader(http.getInputStream()));
                    String line;
                    while ((line = receiver.readLine()) != null){
                        responseRaw.append(line);
                    }
                    receiver.close();

                    if (responseRaw.toString().length() < 500) {
                        System.out.println("RECEIVED: " + responseRaw.toString());
                    }

                    if (responseRaw.toString().contains("YouTube to MP3 Developer API:")){  //this means the exploit has probably been plugged
                        throw new StringIndexOutOfBoundsException("API error. Default webpage returned");

                    }else if (responseRaw.toString().contains("No Streams found")) {
                        throw new StringIndexOutOfBoundsException("No dl links found");

                    } else {
                        Elements buttons = Jsoup.parse(responseRaw.toString()).select(".link");

                        songDlLinks = new String[buttons.size()];
                        for (int i = 0; i < buttons.size(); i++){
                            songDlLinks[buttons.size()-i-1] = "http:" + buttons.get(i).select("a").get(0).attr("href");
                            //reverse button links order
                        }

                        System.out.println("Retrived "+buttons.size()+" urls.");
                    }

                }

            }catch (StringIndexOutOfBoundsException stre){
                stre.printStackTrace();
                if (stre.getMessage().contains("408")){
                    toast(R.string.mp3_server_timeout_msg);
                }else {
                    toast(R.string.mp3_server_issue);
                }
                try {
                    statsHelper.newStat(StatsHelper.ACTION_MP3_SERVER_ISSUE, new JSONObject()
                            .put("message", stre.getMessage())
                            .put("songTitleFull", songTitleFull)
                            .put("ytServiceUrl", ytServiceURL)
                    );
                }catch (JSONException ignored){}
                failProperly(Integer.valueOf(jantools.readPref(SettingsActivity.KEY_SERVER_ERROR)), songToDL);
                continue;

            } catch (IOException e) {
                e.printStackTrace();
                toast(R.string.connection_issue);
                failProperly(Integer.valueOf(jantools.readPref(SettingsActivity.KEY_CONNECTION_ISSUE)), songToDL);
                continue;

            }

            if (safeIsCancelled()){
                failProperly(FAILACT_STOP, songToDL);
                continue;
            }

            //STEP 3: DL File from ytService to device

            String songDlLink;
            if (songDlLinks.length == 4){
                songDlLink = songDlLinks[Integer.valueOf(jantools.readPref(JanTools.PREF_QUALITY))];
            }else if (songDlLinks.length >= 1){
                songDlLink = songDlLinks[0];
            }else{
                toast(R.string.mp3_server_issue);
                failProperly(Integer.valueOf(jantools.readPref(SettingsActivity.KEY_SERVER_ERROR)), songToDL);
                continue;
            }

            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {

                System.out.println("Trying dlLink: " + songDlLink);

                //waiting for the mp3 server to get its shit together before starting dl
                try{
                    connection = waitUntilDownloadable(songDlLink);
                }catch(InterruptedException e){
                    toast(R.string.something_went_terribly_wrong);
                    try {
                        statsHelper.newStat(StatsHelper.ACTION_NB_ERROR, new JSONObject()
                                .put("error", e.getMessage()));
                    }catch (JSONException ignored){}
                    failProperly(FAILACT_STOP, songToDL);
                    continue;
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                String newProg;
                mbs = Math.round(((double) (fileLength / 1_000_000))) + "";
                if (mbs.equals("0")) {
                    newProg = (songTitleFull);
                } else {
                    newProg = (String.format("%s (%sMB)", songTitleFull, mbs));
                }

                sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                        .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_PROG_TXT)
                        .putExtra(EXTRA_NEW_PROG_TXT, newProg)
                );

                if (safeIsCancelled()){
                    failProperly(FAILACT_STOP, songToDL);
                    continue;
                }

                // download the file
                input = connection.getInputStream();

                if (!outputDir.exists()) {
                    if (!outputDir.mkdir()) {

                        sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                                .putExtra(EXTRA_SUBEVENT, SUBEVENT_REQ_STORAGE)
                        );

                        failProperly(FAILACT_STOP, songToDL);
                        continue;
                    }
                }

                File outputFile = getTmpFile();
                File finalFile = getFinalFile();

                if (outputFile.exists()) {
                    outputFile.delete();
                }

                if (finalFile.exists()) {
                    toast(R.string.song_already_dled);
                    retry = false;
                    postExec(songToDL);
                    continue;
                }

                output = new FileOutputStream(outputFile);

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    if (safeIsCancelled()) {
                        input.close();
                        output.close();
                        failProperly(FAILACT_STOP, songToDL);
                        return null;
                    }
                    total += count;
                    // publishing the progress...
                    if (fileLength > 100 * 1024) // only if total length is known and is a song
                        publishProgress(((int) ((total * 100) / fileLength)));
                    output.write(data, 0, count);
                }

                if (outputFile.length() < 100 * 1024) {
                    System.out.println("Received too small a file: " + outputFile.length() + " bytes");
                    deleteIfNecessary(outputFile);

                    toast(R.string.mp3_server_issue);
                    try{
                        statsHelper.newStat(StatsHelper.ACTION_MP3_SERVER_ISSUE, new JSONObject()
                                .put("message", "File size: " + outputFile.length() + " bytes")
                                .put("songTitleFull", songTitleFull)
                                .put("url", songDlLink)
                        );
                    }catch (JSONException ignored){}
                    failProperly(Integer.valueOf(jantools.readPref(SettingsActivity.KEY_SERVER_ERROR)), songToDL);
                    continue;

                } else { //dl success!

                    //STEP 4: set mp3 tags

                    try {

                        MusicMetadata metadata = new MusicMetadata(songToDL[0]);

                        metadata.setSongTitle(songToDL[0]);

                        if (!songToDL[1].equals("-")&&!songToDL[1].equals(""))
                            metadata.setArtist(songToDL[1]);
                        new MyID3().write(outputFile, finalFile, new MyID3().read(outputFile), metadata);

                        outputFile.delete();

                    } catch (Exception e) {
                        e.printStackTrace();
                        System.out.println("Could not add MP3 tags to file");
                        if (outputFile.exists()) {
                            outputFile.renameTo(finalFile);
                        }
                    }

                    if (finalFile != null) {
                        //STEP 5: tag the mp3 file for media scanning

                        sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                                .putExtra(EXTRA_SUBEVENT, SUBEVENT_SCAN_FILE)
                                .putExtra(EXTRA_FILE_PATH, finalFile.getAbsolutePath())
                        );

                    }

                }


            } catch (InterruptedIOException ioe) { //cancelled
                failProperly(FAILACT_STOP, songToDL);
                continue;

            } catch (IOException e) {
                e.printStackTrace();
                System.out.println("FAULT: " + e.getMessage());
                toast(R.string.connection_issue);
                failProperly(Integer.parseInt(jantools.readPref(SettingsActivity.KEY_CONNECTION_ISSUE)), songToDL);
                continue;
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }

            consecutiveFails = 0;
            publishProgress(100);
            retry = false;
            postExec(songToDL);
            //successful exit - pass along downloaded song's title and artist



        }// loop end

        if (!safeIsCancelled())
            toast(R.string.all_downloads_complete);

        sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                .putExtra(EXTRA_NEW_DL_MODE, MainActivity.ActMode.STANDBY)
                .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_DL_MODE)
        );
        System.out.println("All downloads completed.");

        liedjiesHelper.close();
        unregisterReceiver();
        jantools.writePref(JanTools.PREF_ONGOING_DL, "false");
        jantools.writePref(JanTools.PREF_STOP_DL, "false");

        return null;

    }

    private int lastPublishedProgress = -1;
    private void publishProgress(Integer perc){
        if (perc > lastPublishedProgress) {

            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_PROG_BAR)
                    .putExtra(EXTRA_PROG_PERC, perc)
            );

            notifHelper.setNotifProgress(perc);

            lastPublishedProgress = perc;
        }
    }

    private void postExec(String[] song){

        File tmpFile = getTmpFile();
        File finalFile = getFinalFile();

        if (song == null){
            deleteIfNecessary(tmpFile);
            deleteIfNecessary(finalFile);
            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_NEW_DL_MODE, MainActivity.ActMode.STANDBY)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_CHANGE_DL_MODE)
            );
            return;
        }

        if (safeIsCancelled()) { //dws dlMode == false || isCancelled == true
            deleteIfNecessary(tmpFile);
            deleteIfNecessary(finalFile);
            return; //totally cancelled
        }


        if (!retry){    //delete from list

            int songID = liedjiesHelper.getSongID(song[0], song[1]);
            if (songID != -1) {
                liedjiesHelper.getDB().delete(
                        LiedjiesContract.TblLinks.TABLE_NAME,
                        LiedjiesContract.TblLinks.COLUMN_SONG_ID + " =" +songID,
                        null
                );
            }

            liedjiesHelper.getDB().delete(
                    LiedjiesContract.TblLiedjies.TABLE_NAME,
                    LiedjiesContract.TblLiedjies.COLUMN_SONG + " = ? " +
                            "AND " + LiedjiesContract.TblLiedjies.COLUMN_ARTIST + " = ?",
                    song
            );

            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_LIST_UPDATE)
            );

            System.out.println("Download success!");


            JSONObject dlStat = new JSONObject();
            try {
                dlStat.put("title", song[0]);
                dlStat.put("artist", song[1]);
                dlStat.put("size_mb", mbs);
                dlStat.put("dl_time_min", Minutes.minutesBetween(startPoint, new DateTime()).getMinutes());
            }catch (JSONException ignored){}
            statsHelper.newStat(StatsHelper.ACTION_DOWNLOAD, dlStat);

            jantools.writePref(JanTools.PREF_TOTAL_DOWNLOADS,
                    (Integer.valueOf(jantools.readPref(JanTools.PREF_TOTAL_DOWNLOADS)) + Integer.parseInt(mbs)) + ""
            );

        }else{
            deleteIfNecessary(tmpFile);
            deleteIfNecessary(finalFile);
        }

        resetVars();

    }

    private void resetVars(){
        songTitleFull = null; mbs = null;
        lastPublishedProgress = -1;
    }

    private void failProperly(int failMode, String[] params){
        consecutiveFails++;
        if (consecutiveFails >= Integer.valueOf(jantools.readPref(SettingsActivity.KEY_FAILS_TO_STOP))){
            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_FAILS_DIALOG)
            );

            try {
                statsHelper.newStat(StatsHelper.ACTION_FAILS_STOP, new JSONObject().put("fail_count", consecutiveFails)
                        .put("title", params[0])
                        .put("artist", params[1]));
            } catch (JSONException ignored) {}

            consecutiveFails = 0;

            retry = false;
            postExec(null);
            cancel(false);
        }else if (failMode == FAILACT_RETRY){
            retry = true;
            postExec(params);
        }else if (failMode == FAILACT_SKIP){
            liedjiesHelper.moveSongToBottom(params);
            sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                    .putExtra(EXTRA_SUBEVENT, SUBEVENT_LIST_UPDATE)
            );
            retry = true;
            postExec(params);
        }else if (failMode == FAILACT_STOP){
            retry = false;
            postExec(null);
            cancel(false);
        }else{
            System.out.println("Unsupported fail action: " + failMode);
            retry = false;
            postExec(null);
            cancel(false);
        }

    }

    private boolean safeIsCancelled(){
        return isCancelled() || jantools.readPrefBool(JanTools.PREF_STOP_DL);
    }

    protected void onCancelled(){
        deleteIfNecessary(getTmpFile());
        toast(R.string.dl_cancelled);
        liedjiesHelper.close();
        jantools.writePref(JanTools.PREF_ONGOING_DL, "false");
        jantools.writePref(JanTools.PREF_STOP_DL, "false");
        unregisterReceiver();
        System.out.println("Downloader cancelled");
    }

    private void toast(@StringRes int resID){
        sendBroadcast(new Intent(EVENT_PROG_UPDATE)
                .putExtra(EXTRA_SUBEVENT, SUBEVENT_TOAST)
                .putExtra(EXTRA_TOAST_MSG, resID)
        );
    }

    private HttpURLConnection waitUntilDownloadable(String dlUrl) throws InterruptedException, IOException{

        int LOOP_MAX = 20;
        int WAIT_LENGTH = 2200;

        URL url = new URL(dlUrl);

        int fileSize;
        int loops = 1;

        try {

            HttpURLConnection connection;
            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            fileSize = connection.getContentLength();

            while (fileSize <= 0 && loops < LOOP_MAX) {

                connection.disconnect();

                Thread.sleep(WAIT_LENGTH);

                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                fileSize = connection.getContentLength();
                loops++;

            }

            if (loops >= LOOP_MAX){
                System.out.println("Link not downloadable even after "+loops+" loops");
            }else{
                System.out.println("Link downloadable after "+loops+" loops");
            }

            return connection;

        }catch (MalformedURLException ignored){}

        return null;
    }

    private void deleteIfNecessary(File fileToDel){
        if (fileToDel != null){
            if (fileToDel.exists()){
                fileToDel.delete();
            }
        }
    }

    private File getTmpFile(){
        return songTitleFull==null ? null :
                new File(outputDir.getAbsolutePath() + File.separator + fileNameSafe(songTitleFull) + "-tmp.mp3");
    }
    private File getFinalFile(){
        return songTitleFull==null ? null :
                new File(outputDir.getAbsolutePath() + File.separator + fileNameSafe(songTitleFull) + ".mp3");
    }

    public static File getOutputDir(Context context){
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator +
                Environment.DIRECTORY_MUSIC + File.separator + context.getString(R.string.app_name));
    }

    private String fileNameSafe(String toConvert){

        StringBuilder safeName = new StringBuilder();
        for (char character: toConvert.toCharArray()){
            if(Character.isLetter(character) ||
                    Character.isDigit(character) ||
                    ArrayUtils.contains(new char[]{' ', '-', '(', ')', '[', ']'}, character))
                safeName.append(character);
        }

        return safeName.toString();

    }

    private void unregisterReceiver(){
        if (activityRef.get() != null)
            LocalBroadcastManager.getInstance(activityRef.get()).unregisterReceiver(cancelReceiver);
    }

}
