package com.sps.mus.utils;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.annotation.StringRes;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.sps.mus.SettingsActivity;

import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Random;
import java.util.UUID;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Jans Rautenbach on 2015/09/14.
 */

public class JanTools /*extends Activity*/ {

    private Context ctx;
    private SharedPreferences sharedPref;

    public JanTools(Context sCtx){
        ctx = sCtx;
        sharedPref = PreferenceManager.getDefaultSharedPreferences(ctx);

        if (readPref(PREF_PREF_VERSION) == null){
            convertPrefsFromLegacy();
            writePref(PREF_PREF_VERSION, PREFERENCES_VERSION + "");
        }else if (readPrefInt(PREF_PREF_VERSION) < PREFERENCES_VERSION){
            convertPrefsFromLegacy();
            writePref(PREF_PREF_VERSION, PREFERENCES_VERSION + "");
        }

    }

    public void makeToast(final @StringRes int strID, boolean forceOutsideFocus){
        makeToast(ctx.getString(strID), forceOutsideFocus);
    }

    public void makeToast(final String s, boolean forceOutsideFocus){
        if (((Activity) ctx).hasWindowFocus() || forceOutsideFocus) {
            ((Activity) ctx).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (s.length() < 2) {
                        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                    } else if (s.substring(s.length() - 2).equals("/l")) {
                        Toast.makeText(ctx, s.substring(0, s.length() - 2), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(ctx, s, Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }
    public void makeToast(final String s){
        makeToast(s, false);
    }

    static public int indexOfStrArr(String[] array, String search){

        int pos = 0;
        while (!array[pos].equals(search)){
            pos++;
            if (pos >= array.length){
                return -1;
            }
        }
        return pos;

    }

    public static final String PREF_LATEST_VCODE = "PREF_LATEST_VERSION_CODE";
    public static final String PREF_LATEST_VNAME = "PREF_LATEST_VERSION_NAME";
    public static final String PREF_UPDATE_MSG = "PREF_UPDATE_MESSAGE";
    public static final String PREF_LAST_STAT_UPLOAD = "PREF_LAST_STAT_UPLOAD_TIMESTAMP";
    private static final String PREF_GUID = "GUID";
    public static final String PREF_SEARCH_TUT_SHOWN = "PREF_SEARCH_TUTORIAL_DIALOG_SHOWN";
    public static final String PREF_YT_TUT_SHOWN = "PREF_YOUTUBE_TUTORIAL_DIALOG_SHOWN";
    public static final String PREF_SHAZAM_TUT_SHOWN = "PREF_SHAZAM_TUTORIAL_DIALOG_SHOWN";
    public static final String PREF_SOUNDHOUND_TUT_SHOWN = "PREF_SOUNDHOUND_TUTORIAL_DIALOG_SHOWN";
    public static final String PREF_DEVICE_DETES_UPPED = "PREF_DEVICE_DETAILS_UPLOADED";
    public static final String PREF_QUALITY = "PREF_PREFERRED_QUALITY";
    public static final String PREF_PREFER_YT_DIALOG_SHOWN = "PREF_PREFER_YT_DIALOG_SHOWN";
    public static final String PREF_TOTAL_DOWNLOADS = "PREF_TOTAL_MBS_DOWNLOADED";
    public static final String PREF_FEEDBACK_EMAIL = "PREF_LAST_FEEDBACK_EMAIL";
    public static final String PREF_ONGOING_DL = "PREF_DOWNLOAD_ONGOING";
    public static final String PREF_STOP_DL = "PREF_STOP_DOWNLOAD";

    public static final String PREF_PREF_VERSION = "PREFERENCES_VERSION";
    private static final int PREFERENCES_VERSION = 2;

    private String[] prefsAndDefaults = {   //combined for readability
            SettingsActivity.KEY_DOWNLOAD_ON_MOBILE, "false",
            SettingsActivity.KEY_SERVER_ERROR, "1",
            SettingsActivity.KEY_CONNECTION_ISSUE, "0",
            SettingsActivity.KEY_FAILS_TO_STOP, "7",
            PREF_LATEST_VCODE, "0",
            PREF_LATEST_VNAME, "0",
            PREF_LAST_STAT_UPLOAD, "0",
            PREF_SEARCH_TUT_SHOWN, "false",
            PREF_YT_TUT_SHOWN, "false",
            PREF_SHAZAM_TUT_SHOWN, "false",
            PREF_SOUNDHOUND_TUT_SHOWN, "false",
            PREF_DEVICE_DETES_UPPED, "false",
            PREF_QUALITY, "0", //array index in R.array.qualities
            PREF_PREFER_YT_DIALOG_SHOWN, "false",
            PREF_UPDATE_MSG, "",
            PREF_TOTAL_DOWNLOADS, "0",
            PREF_ONGOING_DL, "false",
            PREF_STOP_DL, "false"
    };

    public void setPrefDefaults(){    //sets prefs to default if they have no value
        for (int i = 0; i < prefsAndDefaults.length; i+=2){
            if (readPref(prefsAndDefaults[i]) == null){
                writePref(prefsAndDefaults[i], prefsAndDefaults[i+1]);
            }
        }
    }

    //sets all prefs that have default vals to their default vals
    public void resetPrefs(){
        for (int i = 0; i < prefsAndDefaults.length; i+=2){
            writePref(prefsAndDefaults[i], prefsAndDefaults[i+1]);

        }
    }

    //converts prefs from earlier versions of the app
    private void convertPrefsFromLegacy(){

        switch (readPref(SettingsActivity.KEY_CONNECTION_ISSUE)) {
            case "Retry Download":
                writePref(SettingsActivity.KEY_CONNECTION_ISSUE, "0");
                break;
            case "Skip Song":
                writePref(SettingsActivity.KEY_CONNECTION_ISSUE, "1");
                break;
            case "Stop All Downloads":
                writePref(SettingsActivity.KEY_CONNECTION_ISSUE, "2");
                break;
        }

        switch (readPref(SettingsActivity.KEY_SERVER_ERROR)){
            case "Retry Download":
                writePref(SettingsActivity.KEY_SERVER_ERROR, "0");
                break;
            case "Skip Song":
                writePref(SettingsActivity.KEY_SERVER_ERROR, "1");
                break;
            case "Stop All Downloads":
                writePref(SettingsActivity.KEY_SERVER_ERROR, "2");
                break;
        }

    }

    public String getGUID(){

        String GUID = readPref(PREF_GUID);

        if (GUID == null){
            GUID = UUID.randomUUID().toString();
            writePref(PREF_GUID, GUID);
        }

        return GUID;

    }

    public void writePref(String key, String val){
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, val);
        editor.apply();
    }

    public String readPref(String key) {
        return sharedPref.getString(key, null);
    }

    public boolean readPref(String in, String cmp){
        return readPref(in).equals(cmp);
    }

    public boolean readPrefBool(String in){
        return readPref(in).equalsIgnoreCase("true");
    }

    public int readPrefInt(String key){return Integer.parseInt(readPref(key));}

    public static void printArray(Object[] print){
        for (int i = 0; i < print.length; i++){
            System.out.println(("pos " + i + ": " + print[i]));
        }
    }

    public boolean checkConnection(){
        ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public boolean checkIfWifi(){
        try {
            ConnectivityManager cm = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            return cm.getActiveNetworkInfo().getType() == ConnectivityManager.TYPE_WIFI;
        }catch (NullPointerException e){
            e.printStackTrace();
            return false;
        }
    }

    public float convertPixelsToDp(float px){
        Resources resources = ctx.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return px / (metrics.densityDpi / 160f);
    }

    public static void writeLine(String line, BufferedWriter writer) throws IOException {
        writer.write(line);
        writer.newLine();
        writer.flush();
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();

        return rand.nextInt((max - min) + 1) + min;
    }

    public static String randArr(String[] array){
        return array[randInt(0, array.length-1)];
    }

    public boolean isPackageInstalled(String packagename) {
        PackageManager packageManager = ctx.getPackageManager();
        try {
            packageManager.getPackageInfo(packagename, 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    public ApplicationInfo getAppInfo(String pkg){

        try{
            return ctx.getPackageManager().getApplicationInfo(pkg, 0);
        }catch (PackageManager.NameNotFoundException e){
            System.out.println("Could not find '"+pkg+"'");
            return null;
        }

    }

    public Drawable scaleDrawable(Drawable image, int percent) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, percent, percent, false);
        return new BitmapDrawable(ctx.getResources(), bitmapResized);
    }

    public void hideKeyboard(){
        View view = ((Activity) ctx).getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }else{
            System.out.println("Couldn't hide keyboard");
        }
    }

    public void hideKeyboard(View view){
        if (view != null) {
            InputMethodManager imm = (InputMethodManager) view.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public String getVersionName(){
        try {
            return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionName;
        }catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
            return null;
        }
    }
    public int getVersionCode(){
        try {
            return ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0).versionCode;
        }catch (PackageManager.NameNotFoundException e){
            e.printStackTrace();
            return -1;
        }
    }
}


