package com.sps.mus;

import android.Manifest;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.sps.mus.utils.Downloader;
import com.sps.mus.utils.JanTools;
import com.sps.mus.utils.LiedjiesContract;
import com.sps.mus.utils.LiedjiesHelper;
import com.sps.mus.utils.NotifHelper;
import com.sps.mus.utils.RautenAPI;
import com.sps.mus.utils.StatsHelper;
import org.joda.time.Hours;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.joda.time.DateTime;



public class MainActivity extends AppCompatActivity {

    public enum ActMode {
        DOWNLOADING, STANDBY
    }
    public ActMode actMode;

    private static final int SETTINGS_REQ_CODE = 101;

    private static final String UPDATE_URL = "https://sps.rauten.co.za/mp3-nabber";

    private JanTools jantools;
    private StatsHelper statsHelper;
    private LiedjiesHelper liedjiesHelper;

    private NotifHelper notifHelper;

    private MenuItem updateMenuItem;

    private Downloader downloader = null;

    private int cancelTries = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setSupportActionBar(((Toolbar) findViewById(R.id.toolbar)));

        jantools = new JanTools(this);
        statsHelper = new StatsHelper(this);
        liedjiesHelper = new LiedjiesHelper(this);
        actMode = ActMode.STANDBY;

        setupListView();

        jantools.setPrefDefaults();

        notifHelper = new NotifHelper(this);

        setupFabs();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                broadcastReceiver, new IntentFilter(Downloader.EVENT_PROG_UPDATE)
        );

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

            switch (intent.getStringExtra(Downloader.EXTRA_SUBEVENT)){

                case Downloader.SUBEVENT_LIST_UPDATE:
                    updateLiedjieLys();
                    break;

                case Downloader.SUBEVENT_TOAST:
                    jantools.makeToast(intent.getIntExtra(Downloader.EXTRA_TOAST_MSG, -1), true);
                    break;

                case Downloader.SUBEVENT_CHANGE_DL_MODE:
                    setDlMode(((ActMode) intent.getSerializableExtra(Downloader.EXTRA_NEW_DL_MODE)));
                    break;

                case Downloader.SUBEVENT_CHANGE_PROG_TXT:
                    String txt = intent.getStringExtra(Downloader.EXTRA_NEW_PROG_TXT);
                    if (txt == null) {
                        ((TextView) findViewById(R.id.txtDownloading)).setText(
                                intent.getIntExtra(Downloader.EXTRA_NEW_PROG_RESID, -1)
                        );
                    } else {
                        ((TextView) findViewById(R.id.txtDownloading)).setText(
                                intent.getStringExtra(Downloader.EXTRA_NEW_PROG_TXT)
                        );
                    }
                    break;

                case Downloader.SUBEVENT_REQ_STORAGE:
                    reqStorage();
                    break;

                case Downloader.SUBEVENT_SCAN_FILE:
                    Intent scanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                    scanIntent.setData(Uri.fromFile(new File(intent.getStringExtra(Downloader.EXTRA_FILE_PATH))));
                    sendBroadcast(scanIntent);
                    break;

                case Downloader.SUBEVENT_CHANGE_PROG_BAR:
                    ((ProgressBar) findViewById(R.id.progBar)).setProgress(
                            intent.getIntExtra(Downloader.EXTRA_PROG_PERC, 0)
                    );
                    break;

                case Downloader.SUBEVENT_FAILS_DIALOG:
                    new AlertDialog.Builder(MainActivity.this)
                            .setMessage(R.string.too_many_fails_rationale)
                            .setNeutralButton(android.R.string.ok, null)
                            .show();
                    break;
            }

        }
    };

    private void setupFabs(){

        final FloatingActionMenu fabMenu = (FloatingActionMenu) findViewById(R.id.fab_menu_main);
        fabMenu.setClosedOnTouchOutside(true);
        fabMenu.setIconAnimated(false);
        fabMenu.setOnMenuToggleListener(new FloatingActionMenu.OnMenuToggleListener() {
            @Override
            public void onMenuToggle(boolean opened) {
                if (opened){
                    fabMenu.getMenuIconView().setImageResource(android.R.drawable.ic_menu_edit);
                }else{
                    fabMenu.getMenuIconView().setImageResource(R.drawable.fab_add);
                }
            }
        });
        fabMenu.setOnMenuButtonClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fabMenu.isOpened()) {
                    onManualInputClick();
                    fabMenu.close(true);
                }else{
                    fabMenu.open(true);
                }
            }
        });

        findViewById(R.id.fab_search).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.close(true);
                onSearchClick();
            }
        });

        ApplicationInfo app;
        if ((app = jantools.getAppInfo("com.shazam.android")) != null) {
            FloatingActionButton fab = findViewById(R.id.fab_shazam);
            fab.setImageDrawable(jantools.scaleDrawable(getPackageManager().getApplicationIcon(app), 75));
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (jantools.readPrefBool(JanTools.PREF_SHAZAM_TUT_SHOWN)){
                        startActivity(getPackageManager().getLaunchIntentForPackage("com.shazam.android"));

                    }else{

                        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_tutorial, null);
                        ((TextView) dialogView.findViewById(R.id.txtTutorial)).setText(R.string.shazam_tutorial);
                        ((ImageView) dialogView.findViewById(R.id.imgTutorial)).setImageResource(R.drawable.shazam_tutorial);

                        new AlertDialog.Builder(MainActivity.this)
                                .setView(dialogView)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        startActivity(getPackageManager().getLaunchIntentForPackage("com.shazam.android"));
                                        jantools.writePref(JanTools.PREF_SHAZAM_TUT_SHOWN, "true");

                                    }
                                })
                                .show();

                    }

                    fabMenu.close(true);

                }
            });
        }

        if ((app = jantools.getAppInfo("com.melodis.midomiMusicIdentifier.freemium")) != null) {
            FloatingActionButton fab = findViewById(R.id.fab_soundhound);
            fab.setImageDrawable(jantools.scaleDrawable(getPackageManager().getApplicationIcon(app), 75));
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (jantools.readPrefBool(JanTools.PREF_SOUNDHOUND_TUT_SHOWN)){
                        startActivity(getPackageManager().getLaunchIntentForPackage("com.melodis.midomiMusicIdentifier.freemium"));

                    }else{

                        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_tutorial, null);
                        ((TextView) dialogView.findViewById(R.id.txtTutorial)).setText(R.string.shazam_tutorial);
                        ((ImageView) dialogView.findViewById(R.id.imgTutorial)).setImageResource(R.drawable.soundhound_tutorial);

                        new AlertDialog.Builder(MainActivity.this)
                                .setView(dialogView)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        startActivity(getPackageManager().getLaunchIntentForPackage("com.melodis.midomiMusicIdentifier.freemium"));
                                        jantools.writePref(JanTools.PREF_SOUNDHOUND_TUT_SHOWN, "true");

                                    }
                                })
                                .show();

                    }

                    fabMenu.close(true);

                }
            });
        }

        if ((app = jantools.getAppInfo("com.google.android.youtube")) != null) {
            FloatingActionButton fab = findViewById(R.id.fab_youtube);
            fab.setImageDrawable(jantools.scaleDrawable(getPackageManager().getApplicationIcon(app), 75));
            fab.setVisibility(View.VISIBLE);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (jantools.readPrefBool(JanTools.PREF_YT_TUT_SHOWN)){
                        startActivity(getPackageManager().getLaunchIntentForPackage("com.google.android.youtube"));

                    }else{

                        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_tutorial, null);
                        ((TextView) dialogView.findViewById(R.id.txtTutorial)).setText(R.string.youtube_tutorial);
                        ((ImageView) dialogView.findViewById(R.id.imgTutorial)).setImageResource(R.drawable.youtube_tutorial);

                        new AlertDialog.Builder(MainActivity.this)
                                .setView(dialogView)
                                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {

                                        startActivity(getPackageManager().getLaunchIntentForPackage("com.google.android.youtube"));
                                        jantools.writePref(JanTools.PREF_YT_TUT_SHOWN, "true");

                                    }
                                })
                                .show();

                    }

                    fabMenu.close(true);

                }
            });
        }

    }

    private void setupListView(){
        ((ListView) findViewById(R.id.listLiedjies)).setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                onListItemClick(
                        ((TextView) view.findViewById(R.id.firstLine)).getText().toString(),    //title
                        ((TextView) view.findViewById(R.id.secondLine)).getText().toString()    //artist
                );
            }
        });
    }

    public void updateLiedjieLys(){
        ListView listLiedjies = ((ListView) findViewById(R.id.listLiedjies));

        String[] kolomme = {
                LiedjiesContract.TblLiedjies.COLUMN_SONG,
                LiedjiesContract.TblLiedjies.COLUMN_ARTIST
        };

        Cursor cursor = liedjiesHelper.getDB().query(
                LiedjiesContract.TblLiedjies.TABLE_NAME,                       //table name
                kolomme,                                                        //kolomme
                null,                                                           //WHERE kolomme
                null,                                                           //WHERE params
                null,                                                           //GROUP BY
                null,                                                           //HAVING
                LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED + " DESC"       //ORDER BY
        );

        if (cursor.getCount() == 0){
            listLiedjies.setVisibility(View.INVISIBLE);
            (findViewById(R.id.btnLaaiAf)).setEnabled(false);
            return;
        } else {
            listLiedjies.setVisibility(View.VISIBLE);
            (findViewById(R.id.btnLaaiAf)).setEnabled(true);
        }

        String[][] data = new String[2][cursor.getCount()];
        int i = 0;
        while (cursor.moveToNext()){
            data[0][i] = cursor.getString(cursor.getColumnIndex(LiedjiesContract.TblLiedjies.COLUMN_SONG));
            data[1][i] = cursor.getString(cursor.getColumnIndex(LiedjiesContract.TblLiedjies.COLUMN_ARTIST));
            i++;
        }

        final CustomArrayAdapter adapter = new CustomArrayAdapter(getApplicationContext(), R.layout.list_item, data);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((ListView) findViewById(R.id.listLiedjies)).setAdapter(adapter);
            }
        });

        cursor.close();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (!jantools.readPrefBool(JanTools.PREF_ONGOING_DL))
            liedjiesHelper.close();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
            reqStorage();
        }

        //handle stats
        if (jantools.checkIfWifi()) {
            DateTime lastStatUpload = new DateTime(Long.valueOf(jantools.readPref(JanTools.PREF_LAST_STAT_UPLOAD)));
            if (Hours.hoursBetween(lastStatUpload, new DateTime()).getHours() >= 24) {
                statsHelper.uploadStats();
                jantools.writePref(JanTools.PREF_LAST_STAT_UPLOAD, new Date().getTime() + "");

            } else System.out.println("Stats upload cancelled: " + Hours.hoursBetween(lastStatUpload, new DateTime()).getHours() + " hours since last upload.");

        }else System.out.println("Stats upload cancelled: no wifi");

        if (jantools.readPrefBool(JanTools.PREF_DEVICE_DETES_UPPED)){
            System.out.println("Device detes already upped");
        }else{
            statsHelper.uploadDeviceDetails();
            jantools.writePref(JanTools.PREF_DEVICE_DETES_UPPED, "true");
        }

    }

    @Override
    public void onBackPressed() {

        FloatingActionMenu fabMenu = findViewById(R.id.fab_menu_main);

        if (fabMenu.isOpened()){
            fabMenu.close(true);
        }else{
            finish();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (grantResults[0] == PackageManager.PERMISSION_DENIED){
            new AlertDialog.Builder(this)
                    .setMessage(R.string.perm_denied_explination)
                    .setNeutralButton(R.string.understood_dialog_btn, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            reqStorage();
                        }
                    })
                    .setCancelable(false)
                    .show();
        }else{
            new AlertDialog.Builder(this)
                    .setTitle(R.string.welcome)
                    .setMessage(R.string.greeting)
                    .setNeutralButton(android.R.string.ok, null)
                    .show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        updateMenuItem = menu.findItem(R.id.action_update);

        if (Integer.valueOf(jantools.readPref(JanTools.PREF_LATEST_VCODE)) > jantools.getVersionCode()){
            updateMenuItem.setVisible(true);
            jantools.makeToast(getString(R.string.update_available), true);
            System.out.println("Update available: " + jantools.readPref(JanTools.PREF_LATEST_VNAME));
        }else {
            new VersionChecker().execute();
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.action_settings:

                startActivityForResult(new Intent(MainActivity.this, SettingsActivity.class),
                        SETTINGS_REQ_CODE);
                break;

            case R.id.action_view_downloads:

                Uri selectedUri = Uri.parse(Downloader.getOutputDir(this).getAbsolutePath());
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(selectedUri, "resource/folder");

                boolean success = false;
                if (intent.resolveActivityInfo(getPackageManager(), 0) != null){
                    startActivity(intent);
                    success = true;
                } else {
                    new AlertDialog.Builder(this)
                            .setMessage(getString(R.string.cant_open_folder_rationale) + "\n" +
                                    Downloader.getOutputDir(this).getAbsolutePath())
                            .setNeutralButton(android.R.string.ok, null)
                            .setCancelable(true)
                            .show();
                }

                try {
                    statsHelper.newStat(StatsHelper.ACTION_OPEN_DOWNLOADS, new JSONObject().put("success", success));
                } catch (JSONException ignored) {}

                break;

            case R.id.action_about:

                startActivity(new Intent(this, AboutActivity.class));
                break;

            case R.id.action_feedback:

                startActivity(new Intent(this, FeedbackActivity.class));
                break;

            case R.id.action_update:

                String currVName = jantools.getVersionName();

                new AlertDialog.Builder(this)
                        .setMessage(String.format("%s %s\n%s %s\n%s\n%s",
                                getString(R.string.current_version_str), currVName,
                                getString(R.string.new_version_str), jantools.readPref(JanTools.PREF_LATEST_VNAME),
                                jantools.readPref(JanTools.PREF_UPDATE_MSG),
                                getString(R.string.update_dialog_extra)))
                        .setNegativeButton(R.string.dialog_ignore, null)
                        .setPositiveButton(R.string.dialog_update, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Intent updateIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(UPDATE_URL));
                                startActivity(updateIntent);
                            }
                        })
                        .show();

                break;

        }


        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == SETTINGS_REQ_CODE && resultCode == RESULT_OK) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

    }

    private void onManualInputClick(){

        final View dialogView = getLayoutInflater().inflate(R.layout.dialog_manual_input, null);

        AutoCompleteTextView edtArtist = dialogView.findViewById(R.id.edtSongArtist);
        ArrayAdapter<String> artistsAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, liedjiesHelper.getArtists());
        edtArtist.setAdapter(artistsAdapter);

        new AlertDialog.Builder(this)
                .setView(dialogView)
                .setPositiveButton(R.string.manual_input_ok_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        String title = ((EditText) dialogView.findViewById(R.id.edtSongTitle)).getText().toString();
                        String artist = ((EditText) dialogView.findViewById(R.id.edtSongArtist)).getText().toString();

                        switch (liedjiesHelper.addSongToList(title, artist)){
                            case EMPTY:
                                jantools.makeToast(getString(R.string.input_incomplete), true);
                                break;
                            case ALREADY_EXISTS:
                                jantools.makeToast(getString(R.string.song_already_listed), true);
                                break;
                            case SUCCESS:
                                jantools.makeToast(getString(R.string.song_added), true);

                                updateLiedjieLys();

                                if (!jantools.readPrefBool(JanTools.PREF_PREFER_YT_DIALOG_SHOWN)){
                                    new AlertDialog.Builder(MainActivity.this)
                                            .setMessage(R.string.prefer_yt_dialog)
                                            .setNeutralButton(R.string.btn_noted, null)
                                            .show();
                                    jantools.writePref(JanTools.PREF_PREFER_YT_DIALOG_SHOWN, "true");
                                }
                                break;
                        }

                        jantools.hideKeyboard(dialogView);

                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        jantools.hideKeyboard(dialogView);
                    }
                })
                .show();

    }

    private void onSearchClick() {

        if (jantools.readPrefBool(JanTools.PREF_SEARCH_TUT_SHOWN)){
            startActivity(new Intent(Intent.ACTION_WEB_SEARCH));
        }else {

            final View dialogView = getLayoutInflater().inflate(R.layout.dialog_tutorial, null);

            new AlertDialog.Builder(this)
                    .setView(dialogView)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                            startActivity(new Intent(Intent.ACTION_WEB_SEARCH));
                            jantools.writePref(JanTools.PREF_SEARCH_TUT_SHOWN, "true");

                        }
                    })
                    .show();

        }
    }

    private void onListItemClick(final String title, final String artist){

        new AlertDialog.Builder(this)
                .setMessage(artist+" - "+title+"\n" + getString(R.string.song_selected_question))
                .setNegativeButton(R.string.button_delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        Cursor cursor = liedjiesHelper.getDB().query(
                                LiedjiesContract.TblLiedjies.TABLE_NAME,
                                new String[]{
                                        LiedjiesContract.TblLiedjies.COLUMN_SONG,
                                        LiedjiesContract.TblLiedjies.COLUMN_ARTIST
                                },
                                null,
                                null,
                                null,
                                null,
                                LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED + " DESC"
                        );

                        cursor.moveToFirst();

                        //if song is downloading, stop downloads
                        if (cursor.getString(0).equals(title) && cursor.getString(1).equals(artist))
                            if (actMode == ActMode.DOWNLOADING)
                                findViewById(R.id.btnLaaiAf).performClick();

                        cursor.close();

                        liedjiesHelper.getDB().delete(
                                LiedjiesContract.TblLiedjies.TABLE_NAME,
                                LiedjiesContract.TblLiedjies.COLUMN_SONG + " = ? " +
                                        "AND " + LiedjiesContract.TblLiedjies.COLUMN_ARTIST + " = ?",
                                new String[]{title, artist}
                        );

                        updateLiedjieLys();
                    }
                })
                .setNeutralButton(R.string.button_move_to_top, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ContentValues newDatePair = new ContentValues();
                        newDatePair.put(LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));

                        liedjiesHelper.getDB().update(
                                LiedjiesContract.TblLiedjies.TABLE_NAME,
                                newDatePair,
                                LiedjiesContract.TblLiedjies.COLUMN_SONG + " = ? " +
                                        "AND " + LiedjiesContract.TblLiedjies.COLUMN_ARTIST + " = ?",
                                new String[]{title, artist}
                        );

                        updateLiedjieLys();
                    }
                })
                .setPositiveButton(R.string.button_nevermind, null)
                .show();
    }

    public void onLaaiAfClick(View btn){

        jantools.hideKeyboard();

        if (actMode == ActMode.STANDBY){    //dl

            if (!jantools.readPrefBool(SettingsActivity.KEY_DOWNLOAD_ON_MOBILE) && !jantools.checkIfWifi()) {

                new AlertDialog.Builder(MainActivity.this)
                        .setMessage(R.string.no_wifi_dl_rationale)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                jantools.writePref(SettingsActivity.KEY_DOWNLOAD_ON_MOBILE, "true");
                                findViewById(R.id.btnLaaiAf).performClick();
                            }
                        })
                        .setNegativeButton(android.R.string.no, null)
                        .show();

            }else {

                setDlMode(ActMode.DOWNLOADING);

                ListView liedjieLys = findViewById(R.id.listLiedjies);

                if (liedjiesHelper.getLiedjieDBLen() < 1) {
                    jantools.makeToast(getString(R.string.list_empty));
                    setDlMode(ActMode.STANDBY);
                    return;
                }

                View info = liedjieLys.getAdapter().getView(0, null, liedjieLys);

                String songTitle = ((TextView) info.findViewById(R.id.firstLine)).getText().toString();
                String artist = ((TextView) info.findViewById(R.id.secondLine)).getText().toString();

                downloader = new Downloader(this);
                downloader.execute();

                if (!notifHelper.notifIsVisible) {
                    notifHelper.showNotification();
                }

            }

        }else{  //kanselleer

            if (downloader == null) {
                LocalBroadcastManager.getInstance(this).sendBroadcast(
                        new Intent(Downloader.EVENT_CANCEL)
                );
                jantools.writePref(JanTools.PREF_STOP_DL, "true");
                jantools.writePref(JanTools.PREF_ONGOING_DL, "false");
                setDlMode(ActMode.STANDBY);

            } else {
                downloader.cancel(false);
                //if true: gooi interruptexception en doen nie postexec nie.

                setDlMode(ActMode.STANDBY);
            }


        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        updateLiedjieLys();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        if(hasFocus && actMode != ActMode.DOWNLOADING) {
            if (jantools.readPrefBool(JanTools.PREF_ONGOING_DL) && downloader == null && liedjiesHelper.getLiedjieDBLen() > 0) {
                setDlMode(ActMode.DOWNLOADING);
                ((TextView) findViewById(R.id.txtDownloading)).setText(R.string.dl_backg_prog_txt);
                LocalBroadcastManager.getInstance(this).sendBroadcast(new Intent(Downloader.EVENT_PROG_REQUEST));
            }else{
                jantools.writePref(JanTools.PREF_ONGOING_DL, "false");
                jantools.writePref(JanTools.PREF_STOP_DL, "false");
            }
        }

    }

    public void setDlMode(final ActMode mode){
        if (actMode != mode) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ProgressBar progBar = ((ProgressBar) findViewById(R.id.progBar));
                    Button btn = ((Button) findViewById(R.id.btnLaaiAf));
                    actMode = mode;

                    if (mode == ActMode.DOWNLOADING) {
                        btn.setText(R.string.button_stop_download);
                        btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_cancel_black_24dp, 0, 0, 0);

                        findViewById(R.id.fab_menu_main).setVisibility(View.GONE);

                        progBar.setVisibility(View.VISIBLE);
                        progBar.setProgress(0);
                        findViewById(R.id.txtDownloading).setVisibility(View.VISIBLE);

                    } else {
                        btn.setText(R.string.main_download);
                        btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_file_download_black_24dp, 0, 0, 0);

                        progBar.setVisibility(View.GONE);
                        findViewById(R.id.txtDownloading).setVisibility(View.GONE);

                        findViewById(R.id.fab_menu_main).setVisibility(View.VISIBLE);

                        notifHelper.dismissNotification();
                    }
                }
            });
        }
    }

    public void reqStorage(){
        runOnUiThread(new Runnable() {      //dialog for going to settings page
            @Override
            public void run() {
                ActivityCompat.requestPermissions(MainActivity.this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
        });
    }

    /*private int keywordHits(String iKeywords, String textToSearch){

        String strippedKeywords = "";
        for (int i = 0; i < iKeywords.length(); i++){
            if (Character.isLetter(iKeywords.charAt(i)) || Character.isDigit(iKeywords.charAt(i)) || iKeywords.charAt(i) == ' '){
                strippedKeywords += iKeywords.charAt(i);
            }
        }

        String strippedTextToSearch = "";
        for (int i = 0; i < textToSearch.length(); i++){
            if (Character.isLetter(textToSearch.charAt(i)) || Character.isDigit(textToSearch.charAt(i)) || textToSearch.charAt(i) == ' '){
                strippedTextToSearch += textToSearch.charAt(i);
            }
        }

        String[] keywords = strippedKeywords.split(" ");

        int hitCount = 0;

        for (String keyword: keywords){
            if (strippedTextToSearch.contains(keyword)){
                hitCount++;
            }
        }

        return hitCount;

    }

    private int listOccurrences(int search, int[] list){
        int occurrences = 0;
        for(int item: list){
            if (search == item)
                occurrences++;
        }
        return occurrences;
    }*/


    private class CustomArrayAdapter extends ArrayAdapter<String>{

        private String[][] data;
        private Context ctx;

        CustomArrayAdapter(Context context, int listItemLayout, String[][] array){
            super(context, listItemLayout, array[0]);
            data = array;
            ctx = context;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                LayoutInflater inflater = (LayoutInflater) ctx
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.list_item, parent, false);
            }

            ((TextView) convertView.findViewById(R.id.firstLine)).setText(data[0][position]);   //title
            TextView secondLine = convertView.findViewById(R.id.secondLine);
            if (data[1][position].equals("")){
                secondLine.setText("");
                secondLine.setVisibility(View.GONE);
            } else {
                secondLine.setText(data[1][position]);  //artist
            }

            return convertView;
        }

    }

    private class VersionChecker extends AsyncTask<Void, Void, JSONObject>{

        @Override
        protected JSONObject doInBackground(Void... v) {

            if (!jantools.checkIfWifi()){
                return null;
            }

            try{

                Document resultDoc = Jsoup.connect(RautenAPI.URL_VERSION).get();

                return new JSONObject(resultDoc.select("body").html());

            }catch (JSONException|IOException e){
                System.out.println("Couldn't check for new version: " + e.getMessage());
            }


            return null;
        }

        protected void onPostExecute(JSONObject result) {

            if (result != null){

                try {
                    int latestVCode = result.getInt("versionCode");
                    int currVCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;

                    if (latestVCode > currVCode){

                        jantools.writePref(JanTools.PREF_LATEST_VCODE, latestVCode + "");
                        jantools.writePref(JanTools.PREF_LATEST_VNAME, result.getString("versionName"));
                        if (result.has("updateMessage")) {
                            jantools.writePref(JanTools.PREF_UPDATE_MSG, result.getString("updateMessage"));
                        }
                        updateMenuItem.setVisible(true);
                        jantools.makeToast(getString(R.string.update_available));
                        System.out.println("Update available: " + result.getString("versionName"));
                    }

                }catch (JSONException e){
                    e.printStackTrace();
                }catch (PackageManager.NameNotFoundException ignored){}

            }

        }

    }

}
