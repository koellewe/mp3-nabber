package com.sps.mus.utils;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.sps.mus.MainActivity;
import com.sps.mus.R;

/**
 * Created by koell on 21 Jan 2018.
 */

public class NotifHelper {

    public static final int NOTIF_ID = 1;

    public boolean notifIsVisible = false;

    private NotificationCompat.Builder notifBuilder;
    private NotificationManager notifMan;
    private String gettingReady;

    public NotifHelper(Context context){
        Intent notifIntent = new Intent(context, MainActivity.class);
        PendingIntent pIntent = PendingIntent.getActivity(context, NotifHelper.NOTIF_ID, notifIntent, PendingIntent.FLAG_UPDATE_CURRENT);
        notifBuilder = new NotificationCompat.Builder(context, "com.sps.mus.notifchannel-"+NOTIF_ID)
                .setSmallIcon(R.drawable.ic_downloading_notif)
                .setAutoCancel(false)
                .setContentTitle(context.getString(R.string.dialog_downloading_songs))
                .setContentText("")
                .setOngoing(true)
                .setOnlyAlertOnce(true)
                .setContentIntent(pIntent)
                .setPriority(NotificationCompat.PRIORITY_HIGH);

        notifMan = ((NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE));
        gettingReady = context.getString(R.string.notif_getting_ready_to_dl);
    }

    public void showNotification(){
        gooiNotif(notifBuilder.setContentText(gettingReady).build());
    }
    public void dismissNotification(){
        notifMan.cancelAll();
        notifIsVisible = false;
    }
    private void gooiNotif(Notification notif){

        notifMan.notify(NOTIF_ID, notif);
        notifIsVisible = true;
    }
    public void updateNotification(String update){
        gooiNotif(notifBuilder.setContentText(update).build());
    }
    public void setNotifProgress(int progress){
        gooiNotif(notifBuilder.setProgress(100, progress, false).build());
    }
}
