package com.sps.mus.utils;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.sps.mus.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by koell on 21 Jan 2018.
 */

public class LiedjiesHelper {

    public enum Result {
        SUCCESS, ALREADY_EXISTS, EMPTY
    }

    private SQLiteDatabase liedjiesDB;
    private Context context;

    public LiedjiesHelper(Context context){
        this.context = context;
        liedjiesDB = new LiedjiesDBHelper(context).getWritableDatabase();
    }

    public int getSongID(String title){return getSongID(title, "");}
    //gets ID from tblLiedjies of the song with title and artist
    //returns -1 if not found
    public int getSongID(String title, String artist){

        Cursor cursor = liedjiesDB.query(
                LiedjiesContract.TblLiedjies.TABLE_NAME,
                new String[]{LiedjiesContract.TblLiedjies._ID},
                LiedjiesContract.TblLiedjies.COLUMN_SONG + "=? AND " +
                        LiedjiesContract.TblLiedjies.COLUMN_ARTIST + "=?",
                new String[]{title, artist},
                null, null, null
        );

        if (cursor.getCount() > 0){
            cursor.moveToFirst();
            int id = cursor.getInt(0);
            cursor.close();
            return id;
        }else{
            cursor.close();
            return -1;
        }

    }

    //gets pre-saved url from tblLinks of the song with title and artist
    //returns null if there is no pre-saved url
    public String getPreSavedUrl(String title, String artist){

        int songID = getSongID(title, artist);

        if (songID != -1) {

            Cursor cursor = liedjiesDB.query(
                    LiedjiesContract.TblLinks.TABLE_NAME,
                    new String[]{LiedjiesContract.TblLinks.COLUMN_URL},
                    LiedjiesContract.TblLinks.COLUMN_SONG_ID + "=" + songID,
                    null, null, null, null
            );

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();
                String url = cursor.getString(0);
                cursor.close();
                return url;
            } else {
                cursor.close();
                return null;
            }

        }else{
            return null;
        }

    }

    //saves url for songID
    public void saveURL(int songID, String url){

        if (songID != -1){
            ContentValues vals = new ContentValues();
            vals.put(LiedjiesContract.TblLinks.COLUMN_SONG_ID, songID);
            vals.put(LiedjiesContract.TblLinks.COLUMN_URL, url);
            liedjiesDB.insert(
                    LiedjiesContract.TblLinks.TABLE_NAME,
                    null,
                    vals
            );
        }

    }

    public int getLiedjieDBLen(){
        Cursor lenChecker = liedjiesDB.query(
                LiedjiesContract.TblLiedjies.TABLE_NAME,
                new String[]{LiedjiesContract.TblLiedjies.COLUMN_SONG},
                null, null, null, null, null
        );
        int count = lenChecker.getCount();
        lenChecker.close();
        return count;
    }

    public void moveSongToBottom(String[] params){

        try {
            Cursor results = liedjiesDB.rawQuery(
                    "SELECT MIN(" + LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED + ") " +
                            "FROM " + LiedjiesContract.TblLiedjies.TABLE_NAME + ";",
                    null
            );

            results.moveToFirst();

            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date minDate = formatter.parse(results.getString(0));
            Date lowerDate = new Date(minDate.getTime() - 1000);
            String lowerDateStr = formatter.format(lowerDate);

            ContentValues fieldsToUpdate = new ContentValues();
            fieldsToUpdate.put(LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED, lowerDateStr);

            liedjiesDB.update(
                    LiedjiesContract.TblLiedjies.TABLE_NAME,
                    fieldsToUpdate,
                    LiedjiesContract.TblLiedjies.COLUMN_SONG+"=? AND "+
                            LiedjiesContract.TblLiedjies.COLUMN_ARTIST+"=?",
                    params
            );

            results.close();

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public String[] getTopSong(){

        Cursor cursor = liedjiesDB.query(
                LiedjiesContract.TblLiedjies.TABLE_NAME,
                new String[]{
                        LiedjiesContract.TblLiedjies.COLUMN_SONG,
                        LiedjiesContract.TblLiedjies.COLUMN_ARTIST
                },
                null, null, null, null,
                LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED + " DESC",
                "1"
        );

        cursor.moveToFirst();

        String[] song = new String[]{
                cursor.getString(0),
                cursor.getString(1)
        };

        cursor.close();

        return song;

    }

    public Result addSongToList(String songTitle, String artist) {

        if (songTitle.isEmpty()) {
            return Result.EMPTY;
        }else {

            songTitle = songTitle.trim();
            artist = artist.trim();

            String[] kolomme = {
                    LiedjiesContract.TblLiedjies.COLUMN_SONG,
                    LiedjiesContract.TblLiedjies.COLUMN_ARTIST
            };

            Cursor dupeChecker = liedjiesDB.query(
                    LiedjiesContract.TblLiedjies.TABLE_NAME,
                    kolomme,
                    LiedjiesContract.TblLiedjies.COLUMN_SONG + " = ? " +
                            "AND " + LiedjiesContract.TblLiedjies.COLUMN_ARTIST + " = ?",
                    new String[]{songTitle, artist},
                    null,
                    null,
                    null
            );

            if (dupeChecker.getCount() > 0) {
                dupeChecker.close();
                return Result.ALREADY_EXISTS;
            }
            dupeChecker.close();

            ContentValues vals = new ContentValues();
            vals.put(LiedjiesContract.TblLiedjies.COLUMN_SONG, songTitle);
            vals.put(LiedjiesContract.TblLiedjies.COLUMN_ARTIST, artist);
            vals.put(LiedjiesContract.TblLiedjies.COLUMN_DATE_ADDED, new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date())); //this format is required for sqlite

            liedjiesDB.insert(LiedjiesContract.TblLiedjies.TABLE_NAME, null, vals);

            vals.clear();

            addArtist(artist);

            return Result.SUCCESS;

        }

    }

    public void addArtist(String artist){

        if (!artist.equals("")) {

            Cursor dupeChecker = liedjiesDB.query(
                    LiedjiesContract.TblArtists.TABLE_NAME,
                    new String[]{LiedjiesContract.TblArtists.COLUMN_ARTIST},
                    LiedjiesContract.TblArtists.COLUMN_ARTIST + "=?",
                    new String[]{artist},
                    null, null, null
            );

            if (dupeChecker.getCount() == 0) {

                ContentValues vals = new ContentValues();
                vals.put(LiedjiesContract.TblArtists.COLUMN_ARTIST, artist);

                liedjiesDB.insert(LiedjiesContract.TblArtists.TABLE_NAME, null, vals);

            }

            dupeChecker.close();

        }

    }

    public String[] getArtists(){

        Cursor cursor = liedjiesDB.query(LiedjiesContract.TblArtists.TABLE_NAME,
                new String[]{LiedjiesContract.TblArtists.COLUMN_ARTIST},
                null, null, null, null,
                LiedjiesContract.TblArtists.COLUMN_ARTIST + " ASC");

        String[] artists = new String[cursor.getCount()];

        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++){
            artists[i] = cursor.getString(0);
            cursor.moveToNext();
        }

        cursor.close();

        return artists;

    }

    public SQLiteDatabase getDB(){
        return liedjiesDB;
    }

    public void close(){
        liedjiesDB.close();
    }

}
